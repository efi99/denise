
1.0.5
-----
[drag'n'drop]
Es ist verfügbar im Firmware und Laufwerke Fenster oder direkt auf dem Hauptfenster.
Zieht man eine Datei auf das Hauptfenster und ist die Emulation ausgeschalten, wird das Abbild
in das entsprechende Laufwerk (Diskette, Kasette, Modul, Prg) eingelegt und gestartet.
Das 'Load' Kommando wird ausgeführt und abschließend ein 'Run' getriggert.
Ist die Emulation bereits eingeschalten, wird nur das Abbild eingelegt und das entsprechende Laufwerke Fenster angezeigt.
Die Emulation wird jedoch nicht neu gestartet. Es könnte sich ja um einen Disketten/Kasetten Wechsel handeln.
Unter Einstellungen kann dieses Verhalten derart geändert werden, das die Emulation immer neu startet.
Möchte man nur die Dateien einlegen, sollten diese direkt auf die jeweiligen Eingabe Boxen gezogen werden.
Dies löst in keinen Fall einen Power/Reset Zyklus aus.
In den Eingabe Boxen selber kann nicht geschrieben werden, damit nicht ausversehen Pfade verfälscht werden.
Per Drag'n'Drop können auch Archive verwendet werden. Enthält das Archiv nur eine Datei wird diese automatisch verwendet.
Sollten es mehrere sein, erscheint eine Tree View zwecks Auswahl. Generell gilt für Dateien aus Archiven, das diese nur
schreibgeschützt verwendet werden können. Nach einem Neustart des Emulators werden in Archiven zugewiesene Dateien
wiedergefunden.

[Spielstände]
Datei Pfade der verwendeten Firmware werden jetzt mit gespeichert. Beim Laden des Spielstandes wird geschaut, ob die entsprechende
Firmware bereits eingelegt ist. Andernfalls wird dies getan ohne die aktuell Angezeigte zu überschreiben. Bei einem
Reset wird dann wieder die Aktuelle verwendet. Sollten die Firmware Pfade im Spielstand ungültig sein, wird über eine
Fehlermeldung informiert und der Spielstand mit der aktuellen Firmware geladen.
Warum ist das Speichern der Firmware Pfade wichtig ?
Wird z.B. ein Spielstand beim Laden einer Disk mit aktiviertem Jiffy Dos generiert und dieser später mit der standard Firmware
geladen, knallt es natürlich.
Aufgrund von Änderungen können keine Spielstände, generiert mit der Vorgänger Version von Denise, in der Aktuellen
verwendet werden. Es wird durch eine entsprechende Fehlermeldung abgewiesen.

[Firmware]
Nun können 2 zusätzliche Konfigurationen vorbereitet werden. Per Radio Button kann schnell zwischen den
einzelnen Konfigurationen gewechselt werden. Das erspart das lästige Neu Einlegen einzelner Dateien.
Z.b. kann man eine Konfiguration für Jiffy Dos erstellen. Hierbei reicht es aus Kernal und Floppy Firmware
vorzubereiten. Char und Basic müssen nicht zugewiesen sein. Es werden dann automatisch die standard roms verwendet.
Da einige user darüber gestolpert sind, ist Firmware im laufenden Betrieb nicht mehr ausgegraut. Neu eingelegte Firmware
wird dann beim nächsten Power/Reset angewendet.

[Eingabe Zuweisung]
Wird der Emulator das erste Mal gestartet, heißt es ist noch keine settings.ini angelegt, versucht er die im OS
hinterlegte Sprache zu erkennen und lädt dann die entsprechende Übersetzung und weist das entsprechende Tastatur Layout zu.
Zudem werden alle Eingabegeräte mit einer Standard Belegung versehen. Der Joypad z.B. wird mit den Richtungs Tasten der Tastatur
vorbelegt.
Belegt ein am Control Port eingestöpseltes Gerät die gleichen Eingabe Elemente wie die Tastatur werden diese Elemente
für die Tastatur abgeschalten ohne diese aus der Zuweisungsliste zu entfernen. Wird das Gerät ausgestöpselt oder die Zuweisung
derart angepasst, das keine Doppel Belegung mehr auftritt, werden die jeweiligen Eingabe Elemente der Tastatur wieder aktiviert.
Dies geschieht völlig automatisch und man muss nicht befürchten das Zuweisungen verschwinden. Dies verhindert das z.B. bei einem
Spiel der C64 eine gedrückte Richtungs Taste nicht über Tastatur und Control Port empfängt.

Ich habe einen weiteren Freiheitsgrad hinzugefügt. Für jedes Eingabe Element steht ab sofort eine alternative Belegung
zur Verfügung. Ebenso können hierbei mehrere Einzel Elemente zugewiesen werden, welche und/oder verbunden sind.

Beispiele
---------
Spielstand speichern
  Belegung 1: Alt und F5
  Belegung 2: Joypad Schultertaste oder Ziffernblock Taste

C64 Eingabeaufforderung: Stern Taste drücken
  Belegung 1: shift (links)  und Plus
  Belegung 2: shift (rechts) und Plus

C64 Eingabeaufforderung: Piktogramme der Stern Taste ausgeben
  Auf dem c64 ist die Sterntaste eine Primär Belegung. Mittels gedrückter shift Taste wird ein Piktogram ausgelöst,
  mittels gedrückter Commodore Taste das jeweils 2. Piktogram.

  Auf einer PC Tastatur ist es eine Sekundär Belegung der Plus Taste. Die shift Taste ist also bereits vergeben
  und kann keine Piktogramme mehr auslösen. Die der Commodore Taste zugwiesene Taste würde das 2. Piktogram der 
  Plus Taste, jedoch nicht der Stern Taste auslösen.

  Belegung 1: shift ( links oder rechts ) und Plus
  Belegung 2: irgend eine freie Taste um mit Shift/Commodore Tasten Piktogramme zu erstellen.

Die automatische Zuweisung wurde derart überarbeitet, das eine 2. Belegung mit der linken Shift Taste erscheint. (wenn sinnvoll)
Wer die fehlenden Piktogramme (siehe Beispiel 3) benötigt, muss eine Belegung überschreiben. Da es sehr individuell ist, würde
ich es nicht vorbelegen.

Artefakte bei Semikolon
-----------------------
Hält man in der C64 Eingabeaufforderung die linke Shift Taste gedrückt und drückt zudem die Komma Taste erscheint selten
mal eine eckige Klammer anstatt dem erwarteten Semikolon.
Es klingt wie eine Schutzbehauptung aber das ist kein Emulationsfehler. (auch in Vice zu beobachten)
Auf einer C64 Tastatur ist das Semikolon eine separate Taste. Auf dem deutschen Tastatur Layout ist es eine Sekundär Funktion.
Wird die linke Shift Taste des C64 ebenso mit der linken Shift Taste der Tastatur belegt, passiert Folgendes beim Drücken beider Tasten.
Der C64 empfängt die gedrückte Shift Taste. Es passiert erstmal nix. Zudem wird nun die Komma Taste gedrückt. Der Eingabe Manager
übermittelt dem C64 die gedrückte Semikolon Taste, nicht mehr jedoch die gedrückte Shift Taste. Der C64 Kernal registriert
die gedrückte Shift Taste und im nächsten Moment den Wechsel zur Semikolon Taste. Je nachdem wann er checkt, kommt er zu dem
Schluß das beide Tasten zugleich gedrückt sind und stellt dann das Piktogramm dar.
Eine Möglichkeit das Problem zu beseitigen, ist es entweder Semikolon oder links Shift auf eine freie Taste zu legen.

Wechsel des Eingabe Treiber
---------------------------
Beim Wechsel von Eingabe Treibern wurden mitunter Zuweisungen für Maus und Joypad entfernt, da die Treiber unterschiedliche Id's
für die Geräte vergeben. Dies wird jetzt berücksichtigt. Dadurch sollten keine Zuweisungen beim Wechsel mehr verschwinden. 
Unter Umständen können jedoch Fehlzuweisungen bei Joypads bestehen, z.B. bei mehreren Joypads des selben Types, wenn die Treiber
die angeschlossenen Geräte in unterschiedlicher Reihenfolge einlesen. Dann wären beim Treiber Wechsel Joypad 1 und 2 getauscht.
Joypad Treiber sind hot plug fähig.

weitere Änderungen
------------------
Im Tab Disketten Laufwerke werden jetzt nur so viele Laufwerke gelistet, wie aktuell unter Systemverwaltung angegeben.
Dadurch steht mehr Platz für die Auflistung des Disketteninhaltes zur Verfügung.
Ich habe die Rechtschreibkorrekturen von Arndt eingepflegt. Eine Sache ist mir dabei für folgende Einträge aufgefallen.

blank_memory_image = neues Speicherabbild auswählen ...
blank_disk_image = leeres Diskettenabbild auswählen ...
blank_tape_image = leeres Kassettenabbild auswählen ...

Die Übersetzungsschlüssel sind dynamisch generiert. Für jedes Laufwerk besteht die Möglichkeit ein leeres Medium zu erstellen.
Ausnahme bildet hierbei das 'memory' Laufwerk für PRG files, wo keine leeren Medien erstellt werden, sondern das aktuelle
Programm aus dem C64 Speicher in eine prg Datei kopiert wird. Der Übersetzungsschlüssel ist unter diesem Hintergrund irreführend.
Das muss ich in der Anwendung erst heraustrennen.

Commandline Support
-------------------
Die Reihenfolge von Image Pfaden und Parameter spielt keine Rolle. Image Pfade werden ohne einleitende Parameter hinzugefügt.

erlaubte Parameter:

-vic-6569R3
  pal automatisch aktiv

-vic-8565
  pal automatisch aktiv

-vic-6567R8
  ntsc automatisch aktiv

-vic-8562
  ntsc automatisch aktiv

-sid-6581
-sid-8580

-cia-6526a
  neue Cias

-cia-6526

-debugcart
  für Vice Testbench. Alle Images werden vorher ausgeworfen, so das nur die per Command Line eingelegten aktiv sind.
  Cpu Killer Features werden deaktiviert. multiple Abbilder sind möglich. Manche Test brauchen ein prg und ein d64.

-limitcycles %
  automatisches Beenden nach Anzahl Zyklen, wird jedoch nur einmal pro frame geprüft. Emulator Geschwindigkeit 
  ist entsprechend der Sync Optionen.

-no-driver
  Video, Audio, Input Treiber sind deaktiviert. Emulator läuft mit Max Speed, da sync Optionen nicht greifen.
  sinnvoll für automatisierte tests

-no-gui
  aktiviert -no-driver, GUI Schicht läuft im dummy Modus. (keine Fenster, Widgets)
  Hier ist ein Wort der Warnung angebracht. Wenn kein -limitcycles oder -debugcart in Verwendung ist,
  läßt sich der Prozess nur noch im Task Manager beenden.

---------
Beispiele
---------
- Windows
  START /wait Denise.exe -debugcart -no-gui D:\c64\testprogs\drive\skew\skew.g64 D:\c64\testprogs\drive\skew\skew1.prg
  Return Code: echo %errorlevel%

- OSX
  Denise.app/Contents/MacOS/Denise -debugcart -limitcycles 20000000 -no-gui ~/Emulation/testbench/skew.g64 ~/Emulation/testbench/skew1.prg
  Return Code: echo $?   
  Hinweis: nicht den app container mittels open -W -a Denise --args starten. In dem Fall
  läßt sich der Return Code nicht raushangeln.

- Linux
  ./Denise -debugcart -limitcycles 20000000 -no-gui ~/Emulation/testbench/skew.g64 ~/Emulation/testbench/skew1.prg
  Return Code: echo $?   
  

Video
-----
Grundsätzlich stehen 12 Basis Modes zur Verfügung, welche sich aus 3 Entscheidungen ergeben.
1. Palette oder Farbspektrum
2. PAL oder NTSC
3. CRT: AUS/CPU/GPU

Jeder Modus speichert seine eigene Konfiguration, heißt die Slider Einstellungen werden gemerkt wenn der Modus gewechselt wird. Beim Modus Wechsel wird die aktuelle Konfiguration geladen und die Slider entsprechend angepasst.
Der Sinn des Ganzen ist es lästige Neu Anpassungen zu verhindern, wenn z.B. zwischen Palette und Farbspektrum oder PAL und NTSC gewechselt wird und dafür unterschiedliche Einstellungen verwendet werden.
Mittels der Schaltfläche 'Reset' werden alle relevanten Slider des gerade ausgewählten Modus zurück gesetzt.

Intern werden die Farben nicht als RGB verarbeitet, sondern YUV (PAL) bzw. YIQ (NTSC) encoded.
Ausnahme hierbei ist: Palette an, CRT aus. Hierbei gehen die RGB Farben der Palette direkt an den Video Treiber.
Im Gegensatz zu RGB werden bei YUV/YIQ die Helligkeit (Luminanz / Luma / Y) und Farbigkeit (Chrominanz / Chroma / UV(pal) / IQ(ntsc) ) voneinander getrennt.
Luma ist das Schwarz/Weiß Signal. Verschiedene Spannungen dieser Leitung stehen für die Graustufen. Zudem ist es allein für die Bildschärfe verantwortlich. Chroma hat auf die Bildschärfe keinen Einfluß.
Chroma ist das Farbsignal und wird mittels UV / IQ Koordinaten im Farbkreis beschrieben. Für NTSC ist das Koordinatensystem 33° gedreht.
Beim C64 verlassen alle Farben gleich gesättigt das Gerät, heißt der Radius aller UV Koordinaten ist gleich. Somit reicht es aus,
das der VIC-II neben dem Luminanz Signal nur den Winkel auf dem Farbkreis in Form einer Spannung ausgibt.

Palette
-------
In der linken Ansicht sind initial alle Standard Paletten auswählbar. Diese können nicht verändert werden. Die Video Emulation verwendet die ausgewählte Palette.
Durch die 'Erstellen' Schaltfläche wird die ausgewählte Palette in eine neue kopiert, welche sich nun bearbeiten lässt. Klickt nun einfach auf die Farbe oder den Hex Code.
Entweder wird der Hex Code manuell überschrieben oder die 3 Farbkanäle per Slider angepasst. Läuft der Emulator sind alle Änderungen sofort sichtbar.
Das gilt aber nur, wenn der Emu bei Fokus Verlust nicht pausiert wird, siehe globale Einstellungen. Über die Schatfläche 'Alle Änderungen speichern' werden sofort alle Anpassungen in eine separate Datei abgelegt.
Alternativ werden die Änderungen beim Beenden des Emulators gespeichert, solange die Checkbox 'Beim Beenden speichern' nicht deaktiviert ist.
Die Änderungen sofort zu speichern, schützt vor Datenverlust bei einem unerwarteten Absturz des Emulators.

Farbspektrum
------------
Die Grundlagen dafür sind vollständig von Pepto's neuer Colodore Variante übernommen.
Hier beschreibt er die Zusammenhänge: https://www.pepto.de/projects/colorvic/
Sind die Slider für Phase, Gamma, Kontrast, Sättigung und Helligkeit in der Standard Einstellung, wie nach einem 'Reset', entspricht dies exakt der Colodore Palette.
Es gibt einige Gründe das Farbspektrum einer Palette vorzuziehen.
Alle Farben sind immer nach den gleichen Gesetzmäßigkeiten erstellt. Z.B. die beiden Blau Töne auf dem C64 Start Bildschirm haben das gleiche Chroma aber unterschiedliches Luma.
Verwendet man eine RGB Palette muss dies beim Zurück Rechnen der Farben nicht unbedingt stimmen.
Außerdem wird berücksichtigt, das das PAL CRT Gamma bei 2.8 liegt. Zudem wird berücksichtigt, das VIC-II Chips nur 9 Luminanzen, ältere Chips sogar nur 5, unterscheiden.
Dies läßt sich unter 'neuere VIC-II (9 Luminanzen)' umstellen. Bei Verwendung einer RGB Palette ist diese Option natürlich deaktiviert. Ebenso 'Phase' ist nur für Farbspektrum aktiv.
Dies verschiebt den Startpunkt für die Farbspektrum Berechnung auf dem Farbkreis und soll somit verschiedene VIC-II Fabrikate simulieren. Natürlich sind nur einige wenige Grad realistisch.
Grundsätzlich haben die meisten Slider einen deutlich zu hohen Wirkungsbereich. Die maximale Toleranz ist jedoch sehr subjektiv.

Sättigung
---------
Die Werkseinstellung :-) ist für PAL etwas höher um die später beschriebene Entsättigung auszugleichen.

CRT Emulation (CPU oder GPU)
----------------------------
Grundsätzlich gilt für alle Slider Funktionen, welchen eine Checkbox voran gestellt ist, das diese schnell aktiviert bzw. deaktiviert werden können. Die hat den Vorteil, das der Slider nicht auf 0
gezogen werden muss und später, wenn wieder benötigt, auf den ursprünglichen Wert, den man sowieso vergessen hat.
Die Cpu getriebene CRT Emulation kann und sollte in einem extra Thread ausgeführt werden um die Ressourcen besser auf alle vorhandenen Kerne zu verteilen.
Die Option dafür befindet sich im globalen Bereich des Emulators. Einstellungen -> Video -> extra Thread
Generell gelten alle Optionen unter Einstellungen für alle Emu Cores gleichermaßen. Das ist im Moment unwichtig, da es nur den C64 Core gibt.
Die CRT Emulation des C64 ist hier ganz gut beschrieben: http://hitmen.c02.at/temp/palstuff/

Chroma Subsampling
------------------
Dies passiert automatisch und bedarf keiner Slider Einstellung. Chroma Subsampling kommt aus dem digitalen PAL/NTSC. Im Analogen passiert ein ähnlicher Effekt durch eine verringerte Bandbreite bei der Übertragung.
Hierbei mischen sich die Farben mehrerer benachbarter Pixel. Da Chroma nicht für die Bildschärfe ausschlaggebend ist, nimmt das Auge den Qualitätsverlust nur gering war.

Phasen Fehler
-------------
Bei der Übertragung von Chrominanz entstehen Fehler in der Phase. Dies führt bei NTSC dazu, das die selbe Farbe niemals gleich aussieht.
Aus diesem Grund gibt es eine Einstellung für Tint/Hue um den größten Fehler am TV Gerät auszugleichen. Natürlich können somit nicht die Schwankungen ausgeglichen werden.
Eine Tint Einstellung habe ich nicht vorgesehen. Hierfür wird einfach der Phasen Fehler verändert.

Für PAL wird jede 2. Zeile der V-Wert des Chroma Signals invertiert übertragen. Natürlich schlägt auch hierbei der Phasen Fehler zu. Das hat zur Folge das nach der Übertragung
der Fehler jede 2. Zeile genau in die entgegen gesetzte Richtung ausschlägt. Durch Mittelung beider Zeilen enstehen die Farben der zu übertragenden Zeile ohne Fehler,
wenn man davon ausgeht, das beide Zeilen gleich sind und der Fehler von Zeile zu Zeile nicht nennenswert schwankt (Tatsache). Das PAL System arbeitet auf der Grundlage,
das es mehr Ähnlichkeiten als Unterschiede von Zeile zu Zeile gibt. Zwecks Mittelung wird jede eingehende Bildzeile (Delay Line) im TV gespeichert und dann mit der nächsten eigehenden Zeile gemittelt.
Eine Tint Einstellung ist im PAL TV somit nicht notwendig. Auf diese Weise reduziert sich die vertikale Auflösung. Zudem reduziert sich durch die Mittelung die Sättigung.
Beide Nachteile sind weniger schwerwiegend als schwankende Farben wie im NTSC System. Im Pal System gibt es durch die Mittelung somit mehr als 16 mögliche Farben.
demo Farbmischung für PAL (2. Programm auf der Disk): https://csdb.dk/release/?id=50556
Thread mit Vergleich: https://csdb.dk/forums/?roomid=7&topicid=121173&showallposts=1
In diesem Thread sieht man ganz unten wie das Ergebnis der Demo aussehen sollte. Besonders in dem eingekreisten Bereich sind 2 Farben jeweils um eine Zeile versetzt dargestellt.
Man erkennt es gut, wenn die CRT Emulation deaktiviert ist.
Die Mischfarbe ist dennoch nicht gleich. Das sieht in anderen Emus nicht richtig aus. Möglicherweise habe ich die entsprechenden Einstellungen dort nicht richtig getätigt.

Hanover Bars (nur PAL)
----------------------
Ein Problem gerade bei TV Geräten bis in die 90-iger sind entsättigte Zeilen. Die Gründe dafür sind in der Literatur nicht eindeutig beschrieben. Es liegt wohl am analogen Character der Delay Line.
Decoder späterer PAL CRT's weisen kaum noch entsättigte Zeilen auf.
Der entsprechende Slider hat 2 Funktionsweisen. Negative Prozent Anzeigen am Slider bedeuten eine Entsättigung jeder zweiten Zeile. Positive bedeuten ebenso eine Enstsättingung jeder
zweiten Zeile aber die jeweils andere Zeile wird mit dem selben Wert übersättigt. Dies verstärkt den Effekt.

Scanlines
---------
Um die richtige Anzahl Scanlines hinzuzufügen, wird dies vor der Skalierung in die geforderte Anzeige Auflösung erledigt. Die Anzahl der Bildzeilen bei einem CRT ist jeweils für PAL und NTSC fix.
Größerer CRT's haben nicht mehr Scanlines sondern Dickere. Bei kleinen Auflösungen sollte 'Ganzzahl Skalierung' ebenso zugeschalten werden. Je größer die Auflösung des Ausgabefensters,
desto weniger fallen Unregelmäßigkeiten in den Abständen der Scanlines auf.

Unschärfe (Luma)
----------------
Aufgrund begrenzter Bandbreite bei der Übertragung geht Schärfe verloren und die Helligkeit angrenzender Pixel oszilliert. Dies wird erreicht indem Helligkeit der Nachbar Pixel anteilig mit einberechnet wird.

Luma Anstieg/Abfall
-------------------
Die Schaltkreise des im C64 eingebauten RF Modulators verursachen das Veränderungen in der Luminanz nicht sofort aktiv sind. Es dauert ca. 2 Pixel bis eine Erhöhung der Luminanz ihren finalen Wert erreicht.
Nach einem Pixel wäre der Wert das Mittel aus alter und neuer Luminanz. Das erklärt z.B. beim C64 Start Bildschirm das Verschmieren der beiden Blau Töne an der rechten Bild/Rahmen Grenze.
Ebenso wird die Schrift dadurch dünner, da das dunklere Blau nicht gleich auf das Helle im nächsten Pixel wechselt. Dieser Effekt verursacht also einen weiteren Schärfe Verlust.
Zudem kann die Gesamt Helligkeit, je nach verwendeten Farben, abnehmen. Dies passiert dann wenn die Helligkeit ihren finalen Wert noch nicht erreicht hat und ein dunklerer Pixel dies auch nicht mehr zulässt.
Hierzu gibt es eine Demo: https://csdb.dk/release/?id=81780
Die Schrift kann man nur lesen, wenn RF Modulation zugeschalten ist.

Palette CRT Emulation mit TV Gamma
----------------------------------
Wie bereits erwähnt, ergibt die Farbspektrum Emulation in der Standard Einstellung die Colodore Palette. Wechselt man zwischen der Colodore Palette und der Farbspektrum Emulation ohne aktivierten CRT stimmt die Ausgabe überein.
Schaltet man CRT zu, gibt es eine Abweichung im Gamma. Dies liegt daran, das die Colodore Palette auf ein Gamma von 2.2 (aktuelle Bildschirme) berechnet wurde. Mit diesem Gamma würde sie dann in die CRT Emulation reingehen.
Die Farbspektrum Emulation basiert auf dem PAL Gamma von 2.8 und würde mit diesem Wert in die CRT Emulation eingehen. Bei Aktivierung dieser Option wird die Palette erst auf Gamma 2.8 umgerechnet
und dann durch die CRT Emulation gejagt. Somit stimmt die Ausgabe mit der Farbspektrum Emulation wieder überein. Für NTSC besteht dieses Problem nicht, da NTSC Gamma ebenso wie das Gamma aktueller Monitore bei ca. 2.2 liegt.

Ganzzahl Skalierung
-------------------
Manche Effekte, besonders Scanlines, sehen in beliebigen Auflösungen unschön aus. Die Abstände zwischen den Zeilen sind nicht gleich, wenn die Auflösung kein Vielfaches der Nativen ist.
Bei aktiver Ganzzahl Skalierung wird die vertikale Auflösung des emulierten Systems um einen Ganzzahl Faktor hoch skaliert. z.B. x2, x3, x4 usw. Das funktioniert so:
Die native Auflösung wird verdoppelt und geschaut ob diese noch in das angeforderte Anzeige Fenster passt. Wenn ja verdreifacht usw. Der Bereich, welcher nicht ausgefüllt werden kann, bleibt schwarz.
Die globale Einstellung '4:3 Seitenverhältnis beibehalten' wird bei der Ganzzahl Skalierung stets berücksichtigt. In dem Fall also nicht vergessen das Fenster auch in die Breite zu ziehen.
Wichtig: Die native Auflösung des emulierten Systems ist nicht fix, sondern wird von dem verwendeten Rahmen (der Tab neben Palette) bestimmt. Wenn man so will, wird hier festgelegt wieviel vom Rahmen der TV wegschneidet.
Das was übrig bleibt, ist die native Auflösung, welche die Basis für die Integer Skalierung bildet. Um ein Gefühl zu kriegen, sollte man mit dem Rahmen und der Fenstergröße spielen.
Häufig benötigen externe CRT Shader diese Option um vernünftig auszusehen. Die 'Scanlines' von Denise werden in großen Auflösungen ( >= 3x native Auflösung) auch ganz gut ohne Ganzzahl Skalierung dargestellt.
Ich würde diese Option nicht per se zuschalten.

GPU Shader
----------
Interne GPU Shader können mit Externen kombiniert werden. Externe Shader werden nach den Internen abgearbeitet. Die Reihenfolge externer Shader wird dadurch geregelt,
indem diese in der gewünschten Reihenfolge zugeschalten werden.
Als Grundlage für GPU Shader wird das Bild als YUV/YIQ Signal übermittelt. (float: 32 bit je Farbkanal)
Dies verursacht eine relativ große Datenmenge und ist daher etwas langsamer. Alternativ kann das Bild als 8 bit RGB (8 bit je Farbkanal) übertragen werden. Das stellt bei Verwendung von Farbspektren
theoretisch einen Qualitätsverlust dar. Denn hier muss das YUV Signal erst in RGB kovertiert werden und im Shader wieder zurück in YUV gewandelt werden. In der Praxis
sehe ich nur bei weiser Farbe einen geringen Unterschied.
zu finden in den globalen Einstellungen: unter Einstellungen -> Video -> 32 bit je Farbkanal

CRT Emulation mittels GPU Shader
--------------------------------
Diese Option berechnet die CRT Emulation mittels GPU Shader. Grundsätzlich ist der Ablauf der Gleiche mit folgenden Verbesserungen.
CPU Ressourcen werden freigegeben, da die GPU nun dafür verantwortlich ist. Die GPU hat viele kleine parallele Rechenwerke. Auf diese Weise können viele 100 Pixel parallel berechnet werden
und somit aufwendigere Berechnungen angestellt werden. Wird bei der CPU Variante das Chroma von 4 Pixel gemittelt um die reduzierte Übertragungsbandbreite zu simulieren,
kommt bei der GPU Variante ein Sinc FIR Lowpass Filter zum Einsatz. Luma geht mit einer Bandbreite von 5 Mhz in den Filter ein, PAL Chroma jeweils mit 1.3 Mhz für 'U' und 'V',
NTSC Chroma mit 1.5 Mhz für 'I' und 0.5 MHz für 'Q'.

Folgende Optionen sind nur nutzbar, wenn CRT (GPU) aktiv ist.

FIR Filter Länge
----------------
Die 'Unschärfe' Einstellung im vorherigen Tab ist deaktiviert und durch diese ersetzt. Die Filter Länge bestimmt die Unschärfe. Je länger der Filter, desto mehr vermischen die Pixel ineinander.
Z.b. bei dem Spiel Toki sind schwarze Pixel in den grünen Blättern. Diese sieht man deutlich wenn die CRT Emulation deaktiviert wird. Bei einer Filter Länge von 9 und größer sieht man diese kaum noch.
In der Literatur wird erwähnt, das S-Video die gleichen Bandbreiten für Luma und Chroma bei der Übertragung benutzt wie ein einfaches Antennen Kabel. Bei S-Video wird Luma und Chroma getrennt übertragen
und der TV PAL Decoder muss das Chroma nicht rausfiltern, was zu einer schärferen Darstellung führt.

FIR Filter Schärfe
------------------
Für Schärfe links / rechts sollte die Filter Länge relativ hoch gewählt werden. Bei diesen beiden Varianten wird der Filter auf den rechten bzw. linken Nachbar Pixel konzentriert.

Luma/Chroma Rauschen
--------------------
Hier wird ein zufälliger Fehler eingebaut. Chroma Rauschen macht für NTSC (never the same color) Sinn.

AEC, BA, PHI0, RAS, CAS Luminanz Fehler
---------------------------------------
Besonders bei älteren VIC-II Chips wird das Luma verfälscht, wenn einige Pins ihren Zustand verändern. PHI0, RAS, CAS verhalten sich in allen Zyklen gleich. AEC und BA sind davon abhängig was der VIC-II genau tut.
Wird der VIC-II ohne spezielle Tricks verwendet, verhalten sich auch diese Verfälschungen gleich. Alle 5 Pins wirken dann kombiniert zusammen. Diese Option steht nur für den C64 Core zur Verfügung.
AEC und BA Zustände werden über den unbenutzten Alpha Kanal an den Shader übergeben.
Die träge Luminanz Veränderung der RF Modulation wirkt sich, wie im echten System auch auf die Luminanz Fehler aus. Wird der RF Modulator aus dem C64 ausgebaut, sind die Luminanz Fehler des VIC
stärker wahrnehmbar. Der RF Modulator wirkt sich also positiv auf die Verfälschungen durch diesen VIC-II bug aus.
siehe hier: http://hitmen.c02.at/temp/palstuff/

Bloom (Über Leuchten)
---------------------
CRT's bis in die 90-iger schaffen es aufgrund ihres analogen Characters nicht sauber in der Zeile zu bleiben. Pixel blühen auf und leuchten teilweise in die leeren Zwischenzeilen.
Mit den leeren Zwischenzeilen meine ich die Scanlines. Die Scanlines sind keine schwarzen Zeilen sondern einfach rein gar nix. Der Kathoden Beam strahlt um eine Zeile versetzt.
Letzte Generation von CRT's weist diesen Fehler kaum mehr auf. Das ist auch der Grund, warum Scanlines dort stärker wahrgenommen werden als auf alten Röhren.
'Radius' beschreibt den Bereich für diesen Effekt. 4 Pixel sind empfehlenswert. 'Varianz' beschreibt das Auswaschen dieses Bereiches. Je mehr Radius, desto mehr Einfluss.
'Glühen' ist die eigentliche Einstellung für diesen Effekt und beschreibt wie stark der Pixel aufblüht. 'Gewichtung', wenn zugeschalten, verstärkt diesen Effekt noch.

zufälliger Zeilenversatz
------------------------
ganz miese Antennenkabel (Composite) können schon dazu führen, dass die Bildzeilen leicht hin und her springen.

-------------
Folgende Optionen sind zwar auch über GPU Shader realisiert, sind aber nicht zwingend an die CRT Emulation gekoppelt. In Bezug auf externe Shader werden die folgenden Internen nach möglichen Externen abgearbeitet. 

Licht vom Mittelpunkt
---------------------
Hierüber besteht die Möglichkeit den Bildschirm kreisförmig auszuleuchten um eine abnehmende Helligkeit in den Bild Ecken zu erreichen.

Leuchtkraft
-----------
Hierbei wird die Luminanz im Gegensatz zur Helligkeit am Ende der Darstellungskette angepasst.

Lochmaske
---------
Hierfür habe ich mich ziemlich an Micro64 orientiert. 
In jedem CRT steckt ca. 2 cm hinter der Mattscheibe eine Loch-, Streifen- oder Schlitzmaske. Es gibt 3 Kathodenstrahlen, einen für jede Grundfarbe (RGB). Jeder Bildpunkt auf der Mattscheibe
besteht aus einen Tripel, also 3 Phosphor Punkte für jeweils eine Grundfarbe. Die Intensität der Strahlen regt das Phosphor an und die finale Farbe ergibt sich durch
additive Farbmischung. Das Problem jedoch ist, das der Kathodenstrahl sich nicht ruckartig von einem zum anderen Punkt bewegt und somit für z.B. Grün auch teilweise die rote Phosphor Schicht trifft.
Hier kommt die Lochmaske ins Spiel. Diese sorgt dafür das die richtigen Strahlen auf die entsprechenden Schichten treffen, indem diese an der Maske nur an den entsprechenden Löchern durchgelassen werden.
Die Maske ist jeoch schwach sichtbar. Aus diesem Grund ist sie für die Emulation interessant.
Die beiden gängigsten sind Aperture (Trinitron) und Shadow Mask (Triad).
Der Slider 'Intensität' beschreibt wie intensiv die Lochmaske in die finale Ansicht einfließt. Ist der Slider deaktiviert, sind es die 4 folgenden Optionen ebenso.
'Abstand' beschreibt den Abstand zwischen zwei 'Rot' Durchgängen. (horizontal bei Streifenmaske, diagonal bei Lochmaske).
'DPI' beschreibt wieviel Bildpunkte auf einem Zoll dargestellt sind. Beide Einstellungen ergeben die Körnung und Sichtbarkeit der Maske.
Ich habe für die Textur dieses Shaders MipMapping und anisotropische Filterung zugeschalten.

hohe Auflösung (hires)
----------------------
Hierbei wird die interne Textur verdoppelt. Kombinierte Effekte wie Scanlines + Lochmaske sehen damit in "bestimmten" Auflösungen besser aus.

radiale Verzerrung
------------------
Hiermit läßt sich die Krümmung der Röhre simulieren. Scanlines bei radialer Verzerrung sind ein Thema für sich. Sogenannte Moire patterns sind mehr oder weniger stark zu sehen. 
Besonders bei geringer Auflösung stechen die Verfälschungen hervor. Aus diesem Grund würde ich die Kombination beider features nur bei ca. 4x der nativen Auflösung empfehlen,
da 2x der nativen Auflösung allein schon für die scanlines benötigt wird.

Verzerrung in hoher Auflösung
-----------------------------
Diese Einstellung greift nur, wenn die radiale Verzerrung akiviert ist. Hierbei wird die interne Auflösung genau wie bei 'hires' verdoppelt. Dies kann das Resultat verbessern, wenn
Scanlines und/oder die Lochmaske ebenso zugeschalten sind. Ist 'hires' bereits aktviert, wird die Auflösung ein weiteres Mal verdoppelt.
Das kostet was, aber verbessert das Resultat noch mehr, wenn die Auflösung des Anzeige Fensters entsprechend groß ist. Ohne Scanlines ist diese Option eine Ressourcen Verschwendung.

Interpolationsfilter
--------------------
Unter 'Einstellungen' im Hauptmenu befindet sich der Interpolationsfilter. Der sollte auf 'Linear' stehen. 


Was fehlt noch in Hinsicht auf Shader und Video Treiber ?
---------------------------------------------------------
Die GLSL Shader funktionieren nur mit OpenGL. Ich möchte diese in Slang Shader konvertieren und neben DX9 noch DX10, 11, 12 hinzufügen. Auf diese Weise sind die Shader mit beiden kompatibel.
Dennoch hat man aktuell unter DirectX eine umfangreiche PAL Emulation zur Verfügung. CG Shader und HLSL Shader sind veraltet und spielen für mich keine Rolle.
Neben DirectX und OpenGL bin ich langfristig auch an Vulkan interessiert.

Später möchte ich die Slang Shader von Retro Arch ebenso unterstützen. 
Wichtig dafür ist eine automatisiert generierte Eingabe Maske um die Parameter (defines) externer Shader zur Laufzeit bequem über Slider zu customizen.
CRT-Royale z.B. hat zahlreiche Einstellungsmöglichkeiten. 
Ich habe vor einigen Monaten von Retro Nerd 3 Shader bekommen, Lottes, Trinitron und Trinitron 2. Diese Shader, wie die meisten CRT Shader, kümmern sich nicht um PAL Encoding/Decoding und
natürlich auch nicht um C64 individuelle Anomalien, wie VIC-II Luminanz Fehler oder RF Modulation bedingte Luminanz Fehler. Diese Shader kümmern sich um die physikalischen Eigenschaften der Röhre,
also Scanlines, Bloom und Maske. Besonders das komplexe Zusammenspiel dieser 3 Eigenschaften wird von Shadern, wie CRT-Royale auf einem höherem Level betrieben als bei meiner Version.

Wenn man einen externen CRT Shader verwendet, sollte man diese 3 Eigenschaften in der internen Emulation deaktivieren (Checkboxen), so dass eine reine PAL Signal Emulation
aus interner Sicht läuft. Auf diese Weise stelle ich mir später die Kombination mit anderen RetroArch CRT Shadern vor. Natürlich kann die interne CRT Emulation auch vollständig deaktiviert werden
und nur externe Shader aktiv sein.

@Retro Nerd 
Ich habe diese 3 Shader mit verteilt. Wenn das nicht gewollt ist, nehme ich diese wieder raus. Um die Shader zu benutzen, muss der 'shader' Ordner unter Einstellungen/Video zugewiesen werden.
Dann sind diese 3 Shader unter C64/Shader verfügbar. Beim Shader Wechsel bitte daran denken, dass der alte erst deaktiviert wird, ansonsten sind beide aktiv. Das ist lästig aber dem
Umstand geschuldet, dass mehrere parallel aktiviert werden können.

Wie geht es weiter ?
--------------------
Ich versuche die Kompilierung unter Windows zu vereinfachen.
Das GIT Repo wird öffentlich zugänglich. Fertige Features gehen in den master branch und können dann in Form eines nightlies vor dem offizielen release gebaut werden.
Die settings.ini muss aufgrund der nunmehr vielen Optionen Core separiert und Themen gruppiert gespeichert werden. Dann wären die Cores nicht nur UI technisch voneinander getrennt.

[1.0.6] Das nächste release geht voll umfänglich in den Expansion Port. REU, Easy Flash, ... 
[1.0.7] Danach wird es wieder Zeit für ein Retro Arch Feature. ( frame ahead zur Reduzierung der Eingabe Latenz )
